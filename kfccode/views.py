from django.http import HttpResponse, JsonResponse, HttpResponseBadRequest
from main import get_code_main, get_ff_driver, get_chrome_driver

def get_code_f(request, code):
    if len(code) != 16:
        return HttpResponseBadRequest('Code should be 16 digits')
    for char in code:
        if not str.isdigit(char):
            return HttpResponseBadRequest('Malformed code')
    r_code = get_code_main(code, get_ff_driver)
    return JsonResponse({'code': r_code})

def get_code_c(request, code):
    if len(code) != 16:
        return HttpResponseBadRequest('Code should be 16 digits')
    for char in code:
        if not str.isdigit(char):
            return HttpResponseBadRequest('Malformed code')
    r_code = get_code_main(code, get_chrome_driver)
    return JsonResponse({'code': r_code})