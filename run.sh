#!/bin/bash

IMG="kfc_code"
NAME="kfc_code"

docker rm -f $NAME || :

docker run --detach --rm -e "VIRTUAL_HOST=mm.edwardhuang.nz" -e "LETSENCRYPT_HOST=mm.edwardhuang.nz" \
	--expose 8000 --name $NAME \
	$IMG
