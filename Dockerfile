FROM python:3.8

# install google chrome
RUN apt-get -y update
RUN apt-get install -y chromium

RUN apt-get install -yqq unzip jq tar firefox-esr
RUN wget -O /tmp/chromedriver.zip http://chromedriver.storage.googleapis.com/79.0.3945.36/chromedriver_linux64.zip
RUN unzip /tmp/chromedriver.zip chromedriver -d /usr/local/bin/

RUN curl -s -L $(curl -s https://api.github.com/repos/mozilla/geckodriver/releases/latest | jq -r '.assets[].browser_download_url | select(contains("linux64"))') | tar -xz && chmod +x geckodriver && mv geckodriver '/bin'

# set display port to avoid crash
ENV DISPLAY=:99

RUN pip install selenium django django-cors-headers

COPY . /src

WORKDIR /src

EXPOSE 8000

CMD ["python3", "manage.py", "runserver", "0.0.0.0:8000"]

