from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
import threading
import sys

def get_chrome_driver():
    opts = webdriver.ChromeOptions()
    opts.add_argument('--no-sandbox')
    opts.add_argument('--disable-dev-shm-usage')
    opts.add_argument('--headless')
    opts.add_argument('--window-size=1920,1080')
    driver = webdriver.Chrome(options=opts)
    driver.implicitly_wait = 5

    return driver

def get_ff_driver():
    opts = webdriver.FirefoxOptions()
    opts.add_argument('--headless')
    opts.add_argument('--window-size=1920,1080')
    driver = webdriver.Firefox(options=opts)
    driver.implicitly_wait = 5

    return driver

def get_code_main(code, get_driver_fn=get_ff_driver):

    # Shared memory
    ret = [None]
    so_run = [True]
    th = threading.Thread(target=get_code_main_thread, args=(code, ret, so_run, get_driver_fn))

    th.start()
    th.join(timeout=60)

    if th.is_alive():
        so_run[0] = False
        raise Exception('Operation timed-out')

    if ret[0]:
        if isinstance(ret[0], Exception):
            raise ret[0]
        return ret[0]
    else:
        raise Exception('Thread failed')

def get_code_main_thread(code, mut_list, so_run, get_driver_fn):
    driver = get_driver_fn()

    driver.get('https://s.kfcvisit.com/nzl?AspxAutoDetectCookieSupport=1')
    #nextBtn = driver.find_element_by_id('NextButton')
    #nextBtn.click()

    field = driver.find_element_by_id('InputCouponNum')
    field.send_keys(code)
    nextBtn = driver.find_element_by_id('NextButton')
    nextBtn.click()

    while True:
        if not so_run[0]:
            # stop the thread
            driver.quit()
            return

        errorElements = driver.find_elements_by_class_name('Error')
        if len(errorElements) > 0:
            driver.quit()
            mut_list[0] = Exception('Code is invalid')
            return

        try:
            thing = driver.find_element_by_class_name('radioBranded')
            thing.click()
            nextBtn = driver.find_element_by_id('NextButton')
            nextBtn.click()
        except (Exception):
            pass
        try: 
            thing = driver.find_element_by_tag_name('input')
            thing.send_keys('k')
            nextBtn = driver.find_element_by_id('NextButton')
            nextBtn.click()
        except Exception:
            pass
        try:
            thing = driver.find_element_by_class_name('ValCode')
            data = thing.text.split(' ')[-1]
            driver.quit()
            mut_list[0] = data
            print(data)
            driver.quit()
            return data
        except Exception:
            pass

if __name__ == "__main__":
    l = len(sys.argv)
    if l < 2:
        print('Invalid argv[1]')
    if l >= 3:
        for n in range(sys.argv[1], sys.argv[2]):
            val = get_code_main(n)
            print(n, val)
    else:
        val = get_code_main(sys.argv[1])
        print(sys.argv[1], val)
